package com.wujunshen;

import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Slf4j
@SpringBootApplication
@EnableTransactionManagement
@MapperScan("com.wujunshen.dao")
public class SampleSwaggerSpringBootStarterApplication {

    public static void main(String[] args) {
        log.info("start execute SampleSwaggerSpringBootStarterApplication....\n");
        SpringApplication.run(SampleSwaggerSpringBootStarterApplication.class, args);
        log.info("end execute SampleSwaggerSpringBootStarterApplication....\n");

    }
}