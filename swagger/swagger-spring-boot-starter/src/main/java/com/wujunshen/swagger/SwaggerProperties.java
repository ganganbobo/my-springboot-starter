package com.wujunshen.swagger;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties("swagger")
public class SwaggerProperties {
    /**
     * 开启
     */
    private boolean enable;

    /**
     * 调用上下文路径
     */
    private String contextPath;

    /**
     * 标题
     */
    private String title = "API文档";

    /**
     * 说明
     */
    private String description = "文档内容仅供参考";

    /**
     * 版本
     */
    private String version = "v1.0.0";

    /**
     * 联系人
     */
    private String contact = "****@hotmail.com";
}