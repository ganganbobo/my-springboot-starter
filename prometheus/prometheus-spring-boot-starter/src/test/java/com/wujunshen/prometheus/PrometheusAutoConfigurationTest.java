package com.wujunshen.prometheus;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * @author: frankwoo(吴峻申) <br>
 * @date: 2018/6/15 <br>
 * @time: 21:55 <br>
 * @mail: frank_wjs@hotmail.com <br>
 */
@RunWith(SpringRunner.class)
@SpringBootTest(
        classes = PrometheusAutoConfigurationTest.TestConfig.class,
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        properties = "management.port=0"
)
@Slf4j
public class PrometheusAutoConfigurationTest {

    @Value("${local.management.port}")
    private int managementPort;

    @Test
    public void prometheusAutoConfiguration_whenActive_thenRegistersActuatorEndpoint() {
        // Given
        RestTemplate restTemplate = new RestTemplate();

        log.info("managementPort is:{}", managementPort);
        // When
        ResponseEntity<String> entity =
                restTemplate.getForEntity(
                        "http://localhost:{port}/prometheus", String.class, managementPort);

        // Then
        assertThat(entity.getStatusCodeValue(), is(200));
        assertThat(entity.getBody(), containsString("# HELP heap_used heap_used"));
        assertThat(entity.getBody(), containsString("# TYPE heap_used gauge"));
    }

    @SpringBootApplication
    static class TestConfig {
    }
}