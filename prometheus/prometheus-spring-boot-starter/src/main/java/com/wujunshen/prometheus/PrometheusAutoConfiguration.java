package com.wujunshen.prometheus;

import io.prometheus.client.spring.boot.SpringBootMetricsCollector;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.autoconfigure.ManagementServerProperties;
import org.springframework.boot.actuate.endpoint.PublicMetrics;
import org.springframework.boot.actuate.endpoint.mvc.AbstractMvcEndpoint;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;
import java.util.Collection;

@Configuration
@ConditionalOnWebApplication
@Slf4j
public class PrometheusAutoConfiguration {
    @Resource
    private ManagementServerProperties managementServerProperties;

    @Bean
    @ConditionalOnMissingBean(name = "prometheusActuatorEndpoint")
    public AbstractMvcEndpoint prometheusActuatorEndpoint(
            @Value("${endpoints.prometheus.path:/prometheus}") String path,
            @Value("${endpoints.prometheus.sensitive:false}") boolean sensitive) {
        log.info("\nmanagement.port is:{}\n", managementServerProperties.getPort());

        return new PrometheusEndpoint(path, sensitive);
    }

    @Bean
    @ConditionalOnMissingBean(SpringBootMetricsCollector.class)
    public SpringBootMetricsCollector springBootMetricsCollector(Collection<PublicMetrics> publicMetrics) {
        SpringBootMetricsCollector collector = new SpringBootMetricsCollector(publicMetrics);
        collector.register();
        return collector;
    }
}