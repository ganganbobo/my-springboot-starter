package com.wujunshen.elasticsearch6.wrapper;

import lombok.Data;

/**
 * @author: frankwoo(吴峻申) <br>
 * @date: 2018/7/5 <br>
 * @time: 01:18 <br>
 * @mail: frank_wjs@hotmail.com <br>
 */
@Data
public class QueryPair {
    //查询的字段名字(可多个)
    private String[] fieldNames;
    //查询内容
    private String content;
}