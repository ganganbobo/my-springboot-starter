package com.wujunshen.elasticsearch6;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SampleElasticsearch6SpringBootStarterApplication {

    public static void main(String[] args) {
        SpringApplication.run(SampleElasticsearch6SpringBootStarterApplication.class, args);
    }
}