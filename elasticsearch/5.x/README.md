# elasticsearch5.x版本

由于目前es官方5.x和6.x两个版本同时在维护，所以我们也同时提供两个版本的starter

#### 项目介绍
参见elasticsearch5-spring-boot-starter项目
在/resources/META_INF/spring.factories文件中指定自动装配类

Elasticsearch5AutoConfiguration是自动装配类
装载es的TransportClient类和自定义的Elasticsearch5Template类

#### 使用说明
参见sample-elasticsearch5-spring-boot-starter项目
1. 在pom文件中加入starter依赖
        
```
    <dependency>
        <groupId>com.wujunshen</groupId>
        <artifactId>elasticsearch5-spring-boot-starter</artifactId>
        <version>0.0.1-SNAPSHOT</version>
    </dependency>
    <dependency>
        <groupId>org.elasticsearch</groupId>
        <artifactId>elasticsearch</artifactId>
        <version>5.5.0</version>
    </dependency>        
```

2. 在application.properties配置文件加入下列内容

```
    es.host=localhost
    es.port=9300
    es.httpPort=9201
    es.clusterName=elasticsearch-test
    es.sniff=false
    es.transport.ignoreClusterName=true
    es.transport.pingTimeout=5s
    es.transport.nodesSamplerInterval=5s
```

其中前4行必填，其它可填可不填

另外es.httpPort端口缺省是9200，这里我修改成9201端口

3. 启动sample-elasticsearch5-spring-boot-starter应用后，运行test包下的ESCreateTest和ESSearchTest两个测试类

如下图

![输入图片说明](https://images.gitee.com/uploads/images/2018/0705/120940_0bed0b4f_43183.png "Snip20180705_4.png")