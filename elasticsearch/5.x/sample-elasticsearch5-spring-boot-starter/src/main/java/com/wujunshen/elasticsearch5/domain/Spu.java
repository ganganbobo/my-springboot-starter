package com.wujunshen.elasticsearch5.domain;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class Spu {
    private String productCode;
    private String productName;
    private String brandCode;
    private String brandName;
    private String categoryCode;
    private String categoryName;
    private String imageTag;
    private List<Sku> skus = new ArrayList<>();
}