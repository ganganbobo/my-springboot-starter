package com.wujunshen.elasticsearch5.wrapper;

import lombok.Data;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;

/**
 * @author: frankwoo(吴峻申) <br>
 * @date: 2018/7/5 <br>
 * @time: 11:08 <br>
 * @mail: frank_wjs@hotmail.com <br>
 */
@Data
public class HighLight {
    //高亮字段设置构造器
    private HighlightBuilder builder;
    //高亮字段
    private String field;
}