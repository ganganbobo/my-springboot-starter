package com.wujunshen.elasticsearch5.wrapper;

import lombok.Data;

@Data
public class ESBasicInfo {
    //索引，类似数据库
    private String index;
    //类型，类似表
    private String type;
    //数据ID
    private String[] ids;
}