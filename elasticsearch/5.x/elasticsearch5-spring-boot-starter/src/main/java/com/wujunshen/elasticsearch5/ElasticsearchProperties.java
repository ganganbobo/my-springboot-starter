package com.wujunshen.elasticsearch5;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties("es")
public class ElasticsearchProperties {
    private String host;
    private String port;
    private String httpPort;
    private String clusterName;
    //自动嗅探整个集群的状态，把集群中其他ES节点的ip添加到本地的客户端列表中
    @Value("${es.sniff:true}")
    private String sniff;
}