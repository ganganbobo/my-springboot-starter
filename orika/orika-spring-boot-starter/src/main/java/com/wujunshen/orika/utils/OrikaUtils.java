package com.wujunshen.orika.utils;

import com.wujunshen.orika.convert.EnumConvert;
import com.wujunshen.orika.convert.JsonConvert;
import ma.glasnost.orika.MapperFactory;

/**
 * User:  FrankWoo(吴峻申) <br>
 * Date:  2018/8/13 <br>
 * Time:  17:12 <br>
 * Email: frank_wjs@hotmail.com <br>
 */
public class OrikaUtils {
    public static <D extends Enum<D>> void createEnumConvert(MapperFactory mapperFactory, String convertName, Class<?> dClass) {
        mapperFactory.getConverterFactory().registerConverter(convertName,
                new EnumConvert<D>());
    }

    public static <D> void createJsonConvert(MapperFactory mapperFactory, String convertName, Class<D> dClass) {
        mapperFactory.getConverterFactory().registerConverter(convertName,
                new JsonConvert<D>());
    }
}