package com.wujunshen.orika.convert;

import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.metadata.Type;

/**
 * User:  FrankWoo(吴峻申) <br>
 * Date:  2018/8/13 <br>
 * Time:  15:57 <br>
 * Email: frank_wjs@hotmail.com <br>
 */
public class ValueConverter<T> extends BidirectionalConverter<T, String> {
    @Override
    public String convertTo(T source, Type<String> destinationType, MappingContext mappingContext) {
        return source.toString();
    }

    @Override
    public T convertFrom(String source, Type<T> destinationType, MappingContext mappingContext) {
        return (T) source;
    }
}