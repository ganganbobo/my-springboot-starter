package com.wujunshen.orika.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * User:  FrankWoo(吴峻申) <br>
 * Date:  2018/7/30 <br>
 * Time:  23:52 <br>
 * Email: frank_wjs@hotmail.com <br>
 */
@Data
@ConfigurationProperties("orika")
public class OrikaProperties {
    /**
     * Whether to enable auto-configuration.
     */
    private boolean enabled = true;

    /**
     * Whether to use built-in converters (MapperFactoryBuilder#useBuiltinConverters(boolean)).
     * Follows Orika's behavior by default.
     */
    private boolean useBuiltinConverters = true;

    /**
     * Whether to use auto-mapping (MapperFactoryBuilder#useAutoMapping(boolean)).
     * Follows Orika's behavior by default.
     */
    private boolean useAutoMapping = true;

    /**
     * Whether to map null values (MapperFactoryBuilder#mapNulls(boolean)).
     * Follows Orika's behavior by default.
     */
    private boolean mapNulls = true;

    /**
     * Whether to dump the current state of the mapping infrastructure objects
     * upon occurrence of an exception while mapping (MapperFactoryBuilder#dumpStateOnException(boolean)).
     * Follows Orika's behavior by default.
     */
    private boolean dumpStateOnException = true;

    /**
     * Whether the class-map should be considered 'abstract' (MapperFactoryBuilder#favorExtension(boolean)).
     * Follows Orika's behavior by default.
     */
    private boolean favorExtension = true;

    /**
     * Whether full field context should be captured (MapperFactoryBuilder#captureFieldContext(boolean)).
     * Follows Orika's behavior by default.
     */
    private boolean captureFieldContext = true;

    private String valueConvert = "valueConvert";

    private String dateConvert = "dateConvert";

    private JSONConvertProperties[] json = {};

    private EnumConvertProperties[] enumeration = {};
}