package com.wujunshen.orika.configurer;

import ma.glasnost.orika.MapperFactory;

/**
 * User:  FrankWoo(吴峻申) <br>
 * Date:  2018/7/30 <br>
 * Time:  23:54 <br>
 * Email: frank_wjs@hotmail.com <br>
 */
public interface OrikaMapperFactoryConfigurer {
    /**
     * Configures the {@link MapperFactory}.
     *
     * @param orikaMapperFactory the {@link MapperFactory}.
     */
    void configure(MapperFactory orikaMapperFactory);
}