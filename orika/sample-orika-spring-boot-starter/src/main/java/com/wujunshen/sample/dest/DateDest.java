package com.wujunshen.sample.dest;

import lombok.Data;

/**
 * User:  FrankWoo(吴峻申) <br>
 * Date:  2018/8/13 <br>
 * Time:  14:26 <br>
 * Email: frank_wjs@hotmail.com <br>
 */
@Data
public class DateDest {
    private String createDate;
    private String modifyDate;
}