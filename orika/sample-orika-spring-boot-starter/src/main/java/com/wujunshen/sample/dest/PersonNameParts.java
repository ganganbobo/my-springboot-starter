package com.wujunshen.sample.dest;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class PersonNameParts {
    private String firstName;
    private String lastName;
}