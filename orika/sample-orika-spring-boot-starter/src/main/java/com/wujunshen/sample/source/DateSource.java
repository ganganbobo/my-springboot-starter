package com.wujunshen.sample.source;

import lombok.Data;

import java.util.Date;

/**
 * User:  FrankWoo(吴峻申) <br>
 * Date:  2018/8/13 <br>
 * Time:  14:26 <br>
 * Email: frank_wjs@hotmail.com <br>
 */
@Data
public class DateSource {
    private Date createDate;
    private Date modifyDate;
}