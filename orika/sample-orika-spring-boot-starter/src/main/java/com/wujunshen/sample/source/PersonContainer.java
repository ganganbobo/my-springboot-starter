package com.wujunshen.sample.source;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class PersonContainer {
    private Name name;
}