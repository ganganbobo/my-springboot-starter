package com.wujunshen.sample.dest;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Personne {
    private String nom;
    private String surnom;
}