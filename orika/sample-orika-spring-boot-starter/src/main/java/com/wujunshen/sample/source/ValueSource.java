package com.wujunshen.sample.source;

import lombok.Data;

/**
 * User:  FrankWoo(吴峻申) <br>
 * Date:  2018/8/13 <br>
 * Time:  14:39 <br>
 * Email: frank_wjs@hotmail.com <br>
 */
@Data
public class ValueSource {
    private Object value;
}