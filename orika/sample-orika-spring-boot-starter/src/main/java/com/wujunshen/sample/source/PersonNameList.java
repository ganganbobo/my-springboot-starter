package com.wujunshen.sample.source;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class PersonNameList {
    private List<String> nameList;
}