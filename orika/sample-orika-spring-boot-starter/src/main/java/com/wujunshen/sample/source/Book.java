package com.wujunshen.sample.source;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * User:  FrankWoo(吴峻申) <br>
 * Date:  2018/7/31 <br>
 * Time:  01:13 <br>
 * Email: frank_wjs@hotmail.com <br>
 */
@Data
@AllArgsConstructor
public class Book {
    // 一个Json字符串
    private String bookInformation;
    private int bookType;
}