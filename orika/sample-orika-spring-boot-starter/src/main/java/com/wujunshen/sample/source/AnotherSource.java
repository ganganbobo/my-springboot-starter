package com.wujunshen.sample.source;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * User:  FrankWoo(吴峻申) <br>
 * Date:  2018/8/13 <br>
 * Time:  18:41 <br>
 * Email: frank_wjs@hotmail.com <br>
 */
@Data
@AllArgsConstructor
public class AnotherSource {
    private String anotherName;
    private int anotherAge;
}