package com.wujunshen.sample;

import com.wujunshen.orika.properties.OrikaProperties;
import com.wujunshen.sample.dest.*;
import com.wujunshen.sample.source.*;
import lombok.extern.slf4j.Slf4j;
import ma.glasnost.orika.BoundMapperFacade;
import ma.glasnost.orika.MapperFactory;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.*;

import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

/**
 * @see http://www.baeldung.com/orika-mapping
 */
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = SampleOrikaSpringBootStarterApplication.class)
@EnableAutoConfiguration
public class UnitTest {
    @Resource
    private OrikaProperties orikaProperties;

    @Resource
    private MapperFactory mapperFactory;

    @Test
    public void giveSrcAndDest_WhenMaps_thenCorrect() {
        mapperFactory.classMap(Source.class, Dest.class);
        Source src = new Source("wujunshen", 39);
        Dest actual = mapperFactory.getMapperFacade().map(src, Dest.class);

        log.info("\nname is:{},age is:{}\n", actual.getName(), actual.getAge());

        assertThat(actual.getName(), equalTo(src.getName()));
        assertThat(actual.getAge(), equalTo(src.getAge()));
    }

    @Test
    public void givenSrcAndDest_whenMapsUsingBoundMapper_thenCorrect() {
        BoundMapperFacade<Source, Dest>
                boundMapper = mapperFactory.getMapperFacade(Source.class, Dest.class);
        Source src = new Source("wujunshen", 39);
        Dest actual = boundMapper.map(src);

        log.info("\nname is:{},age is:{}\n", actual.getName(), actual.getAge());
        assertThat(actual.getName(), equalTo(src.getName()));
        assertThat(actual.getAge(), equalTo(src.getAge()));
    }

    @Test
    public void givenSrcAndDest_whenMapsUsingBoundMapperInReverse_thenCorrect() {
        BoundMapperFacade<Source, Dest>
                boundMapper = mapperFactory.getMapperFacade(Source.class, Dest.class);
        Dest src = new Dest("wujunshen", 39);
        Source actual = boundMapper.mapReverse(src);

        log.info("\nname is:{},age is:{}\n", actual.getName(), actual.getAge());
        assertThat(actual.getName(), equalTo(src.getName()));
        assertThat(actual.getAge(), equalTo(src.getAge()));
    }

    @Test
    public void givenSrcAndDestWithDifferentFieldName_whenMaps_thenCorrect() {
        mapperFactory.classMap(Personne.class, Person.class)
                .field("nom", "name")
                .field("surnom", "nickname")
                .register();
        Personne frenchPerson = new Personne("wujunshen", "二少爷");
        Person englishPerson = mapperFactory.getMapperFacade().map(frenchPerson, Person.class);

        log.info("\nname is:{},nickName is:{}\n", englishPerson.getName(),
                englishPerson.getNickname());
        assertThat(englishPerson.getName(), equalTo(frenchPerson.getNom()));
        assertThat(englishPerson.getNickname(), equalTo(frenchPerson.getSurnom()));
    }

    @Test
    public void givenSrcAndDest_byDefault() {
        mapperFactory.classMap(Personne.class, Person.class)
                .field("nom", "name")
                .field("surnom", "nickname")
                .byDefault().register();

        Personne frenchPerson = new Personne("wujunshen", "二少爷");
        Person englishPerson = mapperFactory.getMapperFacade().map(frenchPerson, Person.class);

        log.info("\nname is:{},nickName is:{}\n", englishPerson.getName(),
                englishPerson.getNickname());
        assertThat(englishPerson.getName(), equalTo(frenchPerson.getNom()));
        assertThat(englishPerson.getNickname(), equalTo(frenchPerson.getSurnom()));

        mapperFactory.classMap(Source.class, AnotherSource.class)
                .field("name", "anotherName")
                .field("age", "anotherAge")
                .byDefault().register();

        Source source = new Source("wujunshen", 39);
        AnotherSource anotherSource = mapperFactory.getMapperFacade().map(source, AnotherSource.class);

        log.info("\nname is:{},age is:{}\n", anotherSource.getAnotherName(),
                anotherSource.getAnotherAge());
        assertThat(anotherSource.getAnotherName(), equalTo(source.getName()));
        assertThat(anotherSource.getAnotherAge(), equalTo(source.getAge()));
    }

    @Test
    public void givenSrcWithListAndDestWithPrimitiveAttributes_whenMaps_thenCorrect() {
        mapperFactory.classMap(PersonNameList.class, PersonNameParts.class)
                .field("nameList[0]", "firstName")
                .field("nameList[1]", "lastName").register();
        List<String> nameList = Arrays.asList("frank", "woo");
        PersonNameList src = new PersonNameList(nameList);
        PersonNameParts actual = mapperFactory.getMapperFacade().map(src, PersonNameParts.class);

        log.info("\nfirstName is:{},lastName is:{}\n", actual.getFirstName(),
                actual.getLastName());
        assertThat(actual.getFirstName(), equalTo("frank"));
        assertThat(actual.getLastName(), equalTo("woo"));
    }

    @Test
    public void givenSrcWithMapAndDestWithPrimitiveAttributes_whenMaps_thenCorrect() {
        mapperFactory.classMap(PersonNameMap.class, PersonNameParts.class)
                .field("nameMap['first']", "firstName")
                .field("nameMap['last']", "lastName")
                .register();
        Map<String, String> nameMap = new HashMap<>();
        nameMap.put("first", "frank");
        nameMap.put("last", "woo");
        PersonNameMap src = new PersonNameMap(nameMap);
        //Object -> Class
        PersonNameParts actual = mapperFactory.getMapperFacade().map(src, PersonNameParts.class);

        log.info("\nfirstName is:{},lastName is:{}\n", actual.getFirstName(),
                actual.getLastName());
        assertThat(actual.getFirstName(), equalTo("frank"));
        assertThat(actual.getLastName(), equalTo("woo"));
    }

    @Test
    public void givenSrcWithNestedFields_whenMaps_thenCorrect() {
        mapperFactory.classMap(PersonContainer.class, PersonNameParts.class)
                .field("name.firstName", "firstName")
                .field("name.lastName", "lastName")
                .register();
        PersonContainer src = new PersonContainer(new Name("frank", "woo"));
        PersonNameParts actual = mapperFactory.getMapperFacade().map(src, PersonNameParts.class);

        log.info("\nfirstName is:{},lastName is:{}\n", actual.getFirstName(),
                actual.getLastName());
        assertThat(actual.getFirstName(), equalTo("frank"));
        assertThat(actual.getLastName(), equalTo("woo"));
    }

    @Test
    public void givenSrcWithNullField_whenMapsThenCorrect() {
        mapperFactory.classMap(Source.class, Dest.class).byDefault();

        Source src = new Source(null, 10);
        Dest actual = mapperFactory.getMapperFacade().map(src, Dest.class);

        log.info("\nname is:{},age is:{}\n", actual.getName(), actual.getAge());

        assertThat(actual.getName(), equalTo(src.getName()));
        assertThat(actual.getAge(), equalTo(src.getAge()));
    }

    /**
     * application.properties里设置orika.map-nulls=false时候，
     * source类中属性值为null不会copy到目标类对应的属性中，目标类原有属性值不变
     */
    @Test
    public void givenSrcWithNullAndGlobalConfigForNoNull_whenFailsToMap_ThenCorrect() {
        mapperFactory.classMap(Source.class, Dest.class);
        Source src = new Source(null, 10);
        Dest actual = new Dest("wujunshen", 39);
        mapperFactory.getMapperFacade().map(src, actual);

        log.info("\nname is:{},age is:{}\n", actual.getName(), actual.getAge());

        assertThat(actual.getName(), equalTo("wujunshen"));
        assertThat(actual.getAge(), equalTo(src.getAge()));
    }

    /**
     * application.properties里显式设置orika.map-nulls=true或不设置时，结果与之前相反
     */
    @Ignore
    @Test
    public void givenSrcWithNullAndGlobalConfigForNull_whenFailsToMap_ThenCorrect() {
        mapperFactory.classMap(Source.class, Dest.class);
        Source src = new Source(null, 10);
        Dest actual = new Dest("wujunshen", 39);
        mapperFactory.getMapperFacade().map(src, actual);

        log.info("\nname is:{},age is:{}\n", actual.getName(), actual.getAge());

        assertThat(actual.getName(), nullValue());
        assertThat(actual.getAge(), equalTo(src.getAge()));
    }

    @Test
    public void givenSrcAndDest_whenCanExcludeField_thenCorrect() {
        mapperFactory.classMap(Source.class, TheThirdSource.class).exclude("name")
                .field("age", "anotherAge")
                .register();
        Source source = new Source("wujunshen", 39);
        TheThirdSource theThirdSource = mapperFactory.getMapperFacade().map(source, TheThirdSource.class);

        log.info("\nname is:{},age is:{}\n", theThirdSource.getAnotherName(),
                theThirdSource.getAnotherAge());
        assertThat(theThirdSource.getAnotherName(), nullValue());
        assertThat(theThirdSource.getAnotherAge(), equalTo(source.getAge()));
    }

    /**
     * copy属性是枚举类和json字符串时情形
     */
    @Test
    public void givenSrcWithJSONAndNumField_whenMapsThenCorrect() {
        mapperFactory.classMap(Book.class, BookDest.class)
                .fieldMap("bookType", "bookType")
                .converter(orikaProperties.getEnumeration()[0].getConvertName())
                .add().fieldMap("bookInformation", "bookInfo")
                .converter(orikaProperties.getJson()[0].getConvertName())
                .add().byDefault().register();

        Book src = new Book("{\"ISBN\": \"9787532754687\", \n \"page\": 279\n }",
                new Integer(9).byteValue());

        BookDest actual = mapperFactory.getMapperFacade().map(src, BookDest.class);

        log.info("\nISBN is:{},page is:{},bookType is:{}\n",
                actual.getBookInfo().getIsbn(),
                actual.getBookInfo().getPage(),
                actual.getBookType().getValue());

        assertThat(actual.getBookInfo().getIsbn(), equalTo("9787532754687"));
        assertThat(actual.getBookInfo().getPage(), equalTo(279));
        assertThat(actual.getBookType(), equalTo(BookType.get(9)));
    }

    /**
     * 反向copy属性是枚举类和json字符串时情形
     */
    @Test
    public void givenReverseSrcWithJSONAndNumField_whenMapsThenCorrect() {
        mapperFactory.classMap(BookDest.class, Book.class)
                .fieldMap("bookType", "bookType")
                .converter(orikaProperties.getEnumeration()[0].getConvertName())
                .add().fieldMap("bookInfo", "bookInformation")
                .converter(orikaProperties.getJson()[0].getConvertName())
                .add().byDefault().register();

        BookInfo bookInfo = new BookInfo();
        bookInfo.setIsbn("9787532754687");
        bookInfo.setPage(279);
        BookDest src = new BookDest(BookType.言情, bookInfo);

        Book actual = mapperFactory.getMapperFacade().map(src, Book.class);

        log.info("\nbookInformation is:{},bookType is:{}\n",
                actual.getBookInformation(),
                actual.getBookType());

        assertThat(actual.getBookInformation(), equalTo("{\"ISBN\":\"9787532754687\",\"page\":279}"));
        assertThat(actual.getBookType(), equalTo(9));
    }

    /**
     * copy属性是Date类型时情形
     */
    @Test
    public void givenSrcWithDateField_whenMapsThenCorrect() {
        mapperFactory.classMap(DateSource.class, DateDest.class)
                .fieldMap("createDate", "createDate")
                .converter("dateConvert")
                .add().fieldMap("modifyDate", "modifyDate")
                .converter("dateConvert")
                .add().byDefault().register();

        DateSource dateSource = new DateSource();
        dateSource.setCreateDate(new Date());
        dateSource.setModifyDate(new Date());

        DateDest actual = mapperFactory.getMapperFacade().map(dateSource, DateDest.class);

        log.info("\ncreateDate is:{},modifyDate is:{}\n",
                actual.getCreateDate(),
                actual.getModifyDate());
    }

    /**
     * 反向copy属性是Date类型时情形
     */
    @Test
    public void givenReverseSrcWithDateField_whenMapsThenCorrect() {
        mapperFactory.classMap(DateDest.class, DateSource.class)
                .fieldMap("createDate", "createDate")
                .converter("dateConvert")
                .add().fieldMap("modifyDate", "modifyDate")
                .converter("dateConvert")
                .add().byDefault().register();

        DateDest dateDest = new DateDest();
        dateDest.setCreateDate("2018-08-13 14:33:06");
        dateDest.setModifyDate("2018-08-13 14:33:06");

        DateSource actual = mapperFactory.getMapperFacade().map(dateDest, DateSource.class);

        log.info("\ncreateDate is:{},modifyDate is:{}\n",
                actual.getCreateDate().toString(),
                actual.getModifyDate().toString());
    }

    /**
     * copy属性是Object类型时情形
     */
    @Test
    public void givenSrcWithObjectField_whenMapsThenCorrect() {
        mapperFactory.classMap(ValueSource.class, ValueDest.class)
                .fieldMap("value", "value")
                .converter("valueConvert")
                .add().byDefault().register();

        Map<String, Object> map = new HashMap<String, Object>() {{
            put("maxSize", 100);
        }};

        ValueSource valueSource = new ValueSource();
        valueSource.setValue(map);

        ValueDest actual = mapperFactory.getMapperFacade().map(valueSource, ValueDest.class);

        log.info("\nvalue is:{}\n", actual.getValue());
        assertThat(actual.getValue(), equalTo("{maxSize=100}"));

        valueSource.setValue(8);

        actual = mapperFactory.getMapperFacade().map(valueSource, ValueDest.class);

        log.info("\nvalue is:{}\n", actual.getValue());
        assertThat(actual.getValue(), equalTo("8"));
    }

    /**
     * 反向copy属性是Object类型时情形
     */
    @Test
    public void givenReverseSrcWithObjectField_whenMapsThenCorrect() {
        mapperFactory.classMap(ValueDest.class, ValueSource.class)
                .fieldMap("value", "value")
                .converter("valueConvert")
                .add().byDefault().register();

        ValueDest valueDest = new ValueDest();
        valueDest.setValue("8");

        ValueSource actual = mapperFactory.getMapperFacade().map(valueDest,
                ValueSource.class);

        log.info("\nvalue is:{}\n", actual.getValue());
        assertThat(Integer.parseInt((String) actual.getValue()), equalTo(8));

        Map<String, Object> map = new HashMap<String, Object>() {{
            put("maxSize", 100);
        }};
        valueDest = new ValueDest();
        valueDest.setValue("{maxSize=100}");

        actual = mapperFactory.getMapperFacade().map(valueDest,
                ValueSource.class);

        log.info("\nvalue is:{}\n", actual.getValue());
        assertThat(actual.getValue(), equalTo(map.toString()));
    }
}