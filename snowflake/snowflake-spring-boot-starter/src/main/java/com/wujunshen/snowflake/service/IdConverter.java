package com.wujunshen.snowflake.service;


import com.wujunshen.snowflake.bean.ID;

public interface IdConverter {
    long convert(ID id);

    ID convert(long id);
}