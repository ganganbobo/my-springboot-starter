package com.wujunshen.snowflake.bean;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

/**
 * User:frankwoo(吴峻申) <br>
 * Date:2017/8/24 <br>
 * Time:下午4:29 <br>
 * Mail:frank_wjs@hotmail.com <br>
 */
@Data
public class MakeID {
    @Max(1023)
    @Min(0)
    @JsonProperty("worker")
    private long machine = -1;

    @JsonProperty("timeStamp")
    private long time = -1;

    @Max(4095)
    @Min(0)
    @JsonProperty("sequence")
    private long seq = -1;
}