package com.wujunshen.snowflake;

import com.wujunshen.snowflake.properties.Generate;
import com.wujunshen.snowflake.service.IdService;
import com.wujunshen.snowflake.service.impl.IdServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;

/**
 * User:  FrankWoo(吴峻申) <br>
 * Date:  2018/8/6 <br>
 * Time:  14:59 <br>
 * Email: frank_wjs@hotmail.com <br>
 */
@Slf4j
@Configuration
@EnableConfigurationProperties({Generate.class})
public class SnowFlakeConfiguration {
    @Resource
    private Generate generate;

    @Bean(name = "idService")
    public IdService idService() {
        log.info("worker id is :{}", generate.getWorker());
        return new IdServiceImpl(Long.parseLong(generate.getWorker()));
    }
}