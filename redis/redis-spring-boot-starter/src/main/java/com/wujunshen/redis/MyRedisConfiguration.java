package com.wujunshen.redis;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wujunshen.redis.wrapper.MyRedisTemplate;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.boot.autoconfigure.data.redis.RedisAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisClusterConfiguration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.util.StringUtils;
import redis.clients.jedis.JedisPoolConfig;

import javax.annotation.Resource;

/**
 * @author: frankwoo(吴峻申) <br>
 * @date: 2018/7/10 <br>
 * @time: 18:12 <br>
 * @mail: frank_wjs@hotmail.com <br>
 */
@Slf4j
@Configuration
@AutoConfigureBefore(RedisAutoConfiguration.class)
@EnableConfigurationProperties({RedisProperties.class, RedisPoolProperties.class})
@ConditionalOnClass({MyRedisTemplate.class, RedisTemplate.class, JedisConnectionFactory.class})
public class MyRedisConfiguration extends RedisClusterConfiguration {
    @Resource
    private RedisProperties redisProperties;

    @Resource
    private RedisPoolProperties redisPoolProperties;

    private JedisPoolConfig jedisPoolConfig() {
        JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();

        jedisPoolConfig.setMaxTotal(redisPoolProperties.getMaxActive());
        jedisPoolConfig.setMaxIdle(redisPoolProperties.getMaxIdle());
        jedisPoolConfig.setMinIdle(redisPoolProperties.getMinIdle());
        jedisPoolConfig.setMaxWaitMillis(redisPoolProperties.getMaxWait());

        return jedisPoolConfig;
    }

    private RedisClusterConfiguration redisClusterConfiguration() {
        RedisClusterConfiguration redisClusterConfiguration = new RedisClusterConfiguration(redisProperties.getNodes());

        if (redisProperties.getMaxRedirects() != null) {
            redisClusterConfiguration.setMaxRedirects(redisProperties.getMaxRedirects());
        }

        return redisClusterConfiguration;
    }

    /**
     * 如果配置文件包含“spring.redis”前缀, 则此bean初始化会失效
     * <br>
     * 应用系统会启动报错
     *
     * @return RedisConnectionFactory
     */
    @Bean
    @ConditionalOnExpression("#{!T(com.wujunshen.redis.support.PropertySourcesSupport).containsPropertyNamePrefix(environment, 'spring.redis')}")
    public RedisConnectionFactory connectionFactory() {
        RedisConnectionFactory connectionFactory = new JedisConnectionFactory(redisClusterConfiguration(), jedisPoolConfig());
        ((JedisConnectionFactory) connectionFactory).setTimeout(
                redisProperties.getTimeOut());
        if (!StringUtils.isEmpty(redisProperties.getPassword())) {
            ((JedisConnectionFactory) connectionFactory).setPassword(
                    redisProperties.getPassword());
        }

        return connectionFactory;
    }

    @Bean
    public RedisTemplate<Object, Object> redisTemplate() {
        RedisTemplate<Object, Object> template = new RedisTemplate<>();
        template.setConnectionFactory(connectionFactory());

        //使用Jackson2JsonRedisSerializer来序列化和反序列化redis的value值
        Jackson2JsonRedisSerializer<Object> serializer = new Jackson2JsonRedisSerializer<>(Object.class);

        ObjectMapper mapper = new ObjectMapper();
        mapper.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
        mapper.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
        serializer.setObjectMapper(mapper);

        template.setValueSerializer(serializer);
        //使用StringRedisSerializer来序列化和反序列化redis的key值
        template.setKeySerializer(new StringRedisSerializer());
        template.afterPropertiesSet();
        return template;
    }

    @Bean
    public MyRedisTemplate myRedisTemplate() {
        return new MyRedisTemplate();
    }
}