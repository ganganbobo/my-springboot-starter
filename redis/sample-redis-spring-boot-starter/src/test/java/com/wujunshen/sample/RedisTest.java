package com.wujunshen.sample;

import com.wujunshen.redis.wrapper.MyRedisTemplate;
import lombok.extern.slf4j.Slf4j;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.connection.SortParameters;
import org.springframework.data.redis.core.query.SortQueryBuilder;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.*;

import static java.lang.Thread.sleep;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = SampleRedisSpringBootStarterApplicationTests.class)
@EnableAutoConfiguration
public class RedisTest {
    private static final int MAX_SIZE = 100;
    @Resource
    private MyRedisTemplate myRedisTemplate;

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testFlushDB() {
        assertThat(myRedisTemplate.flushDB(), is("ok"));
    }

    @Test
    public void testGetValuesByKey() {
        assertThat(myRedisTemplate.getSizeOfList("*"), is(0L));
    }

    @Test
    public void testSet() {
        assertThat(myRedisTemplate.exists("lover"), is(false));
        myRedisTemplate.set("lover", "xxxx");
        assertThat(myRedisTemplate.getBy("lover"), is("xxxx"));
        List<Object> keys = myRedisTemplate.getAllValuesBy("lo*");
        assertThat(keys.size(), is(1));
        assertThat(myRedisTemplate.exists("lover"), is(true));
        myRedisTemplate.delete("lover");
    }

    @Test
    public void testSetIfAbsent() {
        assertThat(myRedisTemplate.exists("myName"), is(false));
        myRedisTemplate.setIfAbsent("myName", "frank");
        assertThat(myRedisTemplate.getBy("myName"), is("frank"));
        myRedisTemplate.delete("myName");
    }

    @Test
    public void testAppend() {
        myRedisTemplate.set("myName", "frank");
        assertThat(myRedisTemplate.getBy("myName"), is("frank"));
        assertNotEquals(myRedisTemplate.append("myName", " is me"), 0);
        myRedisTemplate.delete("myName");
    }

    @Test
    public void testSetExpire() throws Exception {
        myRedisTemplate.setExpire("myName", "frank is me", 2L);
        sleep(3000);
        assertNull(myRedisTemplate.getBy("myName"));
        myRedisTemplate.delete("myName");
    }

    @Test
    public void testGetAndSet() {
        myRedisTemplate.set("myName", "frank");
        myRedisTemplate.getAndSet("myName", "wujunshen");
        assertThat(myRedisTemplate.getBy("myName"), is("wujunshen"));
        myRedisTemplate.delete("myName");
    }

    @Test
    public void testGetRangeOf() {
        myRedisTemplate.set("myName", "wujunshen");
        assertThat(myRedisTemplate.getRangeOf("myName", 1, 2), is("wu"));
        myRedisTemplate.delete("myName");
    }

    @Test
    public void testMultiGet() {
        Map<String, String> map = new HashMap<>();
        map.put("key1", "value1");
        map.put("key2", "value2");
        map.put("key3", "value3");
        map.put("key4", "value4");
        myRedisTemplate.multiSet(map);
        Set<Object> set = new HashSet<>();
        set.add("key1");
        set.add("key2");
        set.add("key3");
        set.add("key4");
        assertThat(myRedisTemplate.multiGet(set).size(), is(4));
        assertThat(myRedisTemplate.multiGet(set).get(0), is("value1"));
        myRedisTemplate.deleteMulti(set);
        for (Object value : myRedisTemplate.multiGet(set)) {
            assertNull(value);
        }
    }

    @Test
    public void testGetListBy() {
        myRedisTemplate.addElementOfList("messages", "value1");
        myRedisTemplate.addElementOfList("messages", "value2");
        myRedisTemplate.addElementOfList("messages", "value3");

        List<Object> result1 = myRedisTemplate.getListBy("messages", 0, 1);
        List<Object> result2 = myRedisTemplate.getListBy("messages", 0, -1);
        assertThat(result1.get(0), is("value3"));
        assertThat(result1.get(1), is("value2"));

        assertThat(result2.get(0), is("value3"));
        assertThat(result2.get(1), is("value2"));
        assertThat(result2.get(2), is("value1"));

        myRedisTemplate.delete("messages");
    }

    @Test
    public void testGetSizeBy() {
        myRedisTemplate.addElementOfList("messages", "value1");
        myRedisTemplate.addElementOfList("messages", "value2");
        myRedisTemplate.addElementOfList("messages", "value3");

        assertThat(myRedisTemplate.getSizeOfList("messages"), is(3L));

        myRedisTemplate.delete("messages");
    }

    @Test
    public void testSortBy() {
        myRedisTemplate.addElementOfList("messages", "frank");
        myRedisTemplate.addElementOfList("messages", "alex");
        myRedisTemplate.addElementOfList("messages", "sam");
        myRedisTemplate.addElementOfList("messages", "hydra");
        myRedisTemplate.addElementOfList("messages", "iron man");

        List<Object> result = myRedisTemplate.sortBy("messages");
        assertThat(result.get(0), is("alex"));
        assertThat(result.get(1), is("frank"));
        assertThat(result.get(2), is("hydra"));
        assertThat(result.get(3), is("iron man"));
        assertThat(result.get(4), is("sam"));

        result = myRedisTemplate.sortDescBy("messages");
        assertThat(result.get(0), is("sam"));
        assertThat(result.get(1), is("iron man"));
        assertThat(result.get(2), is("hydra"));
        assertThat(result.get(3), is("frank"));
        assertThat(result.get(4), is("alex"));

        result = myRedisTemplate.sortPageBy("messages", 0, 2);
        assertThat(result.get(0), is("alex"));
        assertThat(result.get(1), is("frank"));

        result = myRedisTemplate.sortPageBy("messages", 1, 2);
        assertThat(result.get(0), is("frank"));
        myRedisTemplate.delete("messages");
    }

    @Test
    public void testGetRangeOfList() {
        myRedisTemplate.addElementOfList("messages", "frank");
        myRedisTemplate.addElementOfList("messages", "alex");
        myRedisTemplate.addElementOfList("messages", "sam");
        myRedisTemplate.addElementOfList("messages", "hydra");
        myRedisTemplate.addElementOfList("messages", "iron man");

        List result = myRedisTemplate.getRangeOfList("messages", 0, 1);
        List result1 = myRedisTemplate.getRangeOfList("messages", 0, -1);

        assertThat(result.get(0), is("iron man"));
        assertThat(result.get(1), is("hydra"));
        assertThat(result1.get(2), is("sam"));
        assertThat(result1.get(3), is("alex"));
        assertThat(result1.get(4), is("frank"));
        myRedisTemplate.delete("messages");
    }

    @Test
    public void testSetElementOfList() {
        myRedisTemplate.addElementOfList("messages", "frank");
        myRedisTemplate.addElementOfList("messages", "alex");
        myRedisTemplate.addElementOfList("messages", "sam");
        myRedisTemplate.addElementOfList("messages", "hydra");
        myRedisTemplate.addElementOfList("messages", "iron man");

        myRedisTemplate.setElementOfList("messages", "吴峻申", 4);
        assertThat(myRedisTemplate.getElementOfList("messages", 4), is("吴峻申"));
        myRedisTemplate.delete("messages");
    }

    @Test
    public void testRemoveElementOfList() {
        myRedisTemplate.addElementOfList("messages", "frank");
        myRedisTemplate.addElementOfList("messages", "alex");
        myRedisTemplate.addElementOfList("messages", "sam");
        myRedisTemplate.addElementOfList("messages", "hydra");
        myRedisTemplate.addElementOfList("messages", "iron man");

        myRedisTemplate.removeElementOfList("messages", "frank", 4);

        List<Object> result = myRedisTemplate.getListBy("messages", 0, -1);
        assertThat(result.size(), is(4));

        myRedisTemplate.delete("messages");
    }

    @Test
    public void testRemoveElementOfListExcept() {
        myRedisTemplate.addElementOfList("messages", "frank");
        myRedisTemplate.addElementOfList("messages", "alex");
        myRedisTemplate.addElementOfList("messages", "sam");
        myRedisTemplate.addElementOfList("messages", "hydra");
        myRedisTemplate.addElementOfList("messages", "iron man");

        myRedisTemplate.removeElementOfListExcept("messages", 0, 1);

        List<Object> result = myRedisTemplate.getListBy("messages", 0, -1);
        assertThat(result.size(), is(2));

        myRedisTemplate.delete("messages");
    }

    @Test
    public void testGetElementBy() {
        myRedisTemplate.addElementOfList("messages", "frank");
        myRedisTemplate.addElementOfList("messages", "alex");
        myRedisTemplate.addElementOfList("messages", "sam");
        myRedisTemplate.addElementOfList("messages", "hydra");
        myRedisTemplate.addElementOfList("messages", "iron man");

        assertThat(myRedisTemplate.getElementOfListBy("messages"), is("frank"));

        myRedisTemplate.delete("messages");
    }

    @Test
    public void testAddElementOfZSet() {
        myRedisTemplate.addElementOfZSet("hackers", "Alan Kay", 1940);
        myRedisTemplate.addElementOfZSet("hackers", "Richard Stallman", 1953);
        myRedisTemplate.addElementOfZSet("hackers", "Yukihiro Matsumoto", 1965);
        myRedisTemplate.addElementOfZSet("hackers", "Claude Shannon", 1916);
        myRedisTemplate.addElementOfZSet("hackers", "Linus Torvalds", 1969);
        myRedisTemplate.addElementOfZSet("hackers", "Alan Turing", 1912);

        Set<Object> mySet = myRedisTemplate.getElementOfZSet("hackers", 0, -1);
        assertThat(mySet.contains("Alan Turing"), is(true));
        assertThat(mySet.contains("Richard Stallman"), is(true));
        assertThat(mySet.contains("Yukihiro Matsumoto"), is(true));
        assertThat(mySet.contains("Linus Torvalds"), is(true));
        assertThat(mySet.contains("Alan Kay"), is(true));

        Set<Object> mySet1 = myRedisTemplate.getElementOfZSetDesc("hackers", 0, -1);
        assertThat(mySet1.contains("Alan Turing"), is(true));
        assertThat(mySet1.contains("Richard Stallman"), is(true));
        assertThat(mySet1.contains("Yukihiro Matsumoto"), is(true));
        assertThat(mySet1.contains("Linus Torvalds"), is(true));
        assertThat(mySet1.contains("Alan Kay"), is(true));

        myRedisTemplate.delete("hackers");
    }

    @Test
    public void testGetSizeOfZSet() {
        myRedisTemplate.addElementOfZSet("hackers", "Alan Kay", 1940);
        myRedisTemplate.addElementOfZSet("hackers", "Richard Stallman", 1953);
        myRedisTemplate.addElementOfZSet("hackers", "Yukihiro Matsumoto", 1965);
        myRedisTemplate.addElementOfZSet("hackers", "Claude Shannon", 1916);
        myRedisTemplate.addElementOfZSet("hackers", "Linus Torvalds", 1969);
        myRedisTemplate.addElementOfZSet("hackers", "Alan Turing", 1912);

        assertThat(myRedisTemplate.getSizeOfZSet("hackers"), is(6L));
        myRedisTemplate.delete("hackers");
    }

    @Test
    public void testGetScoreOfZSet() {
        myRedisTemplate.addElementOfZSet("hackers", "Alan Kay", 1940);
        myRedisTemplate.addElementOfZSet("hackers", "Richard Stallman", 1953);
        myRedisTemplate.addElementOfZSet("hackers", "Yukihiro Matsumoto", 1965);
        myRedisTemplate.addElementOfZSet("hackers", "Claude Shannon", 1916);
        myRedisTemplate.addElementOfZSet("hackers", "Linus Torvalds", 1969);
        myRedisTemplate.addElementOfZSet("hackers", "Alan Turing", 1912);

        assertThat(myRedisTemplate.getScoreOfZSet("hackers", "Yukihiro Matsumoto"), is(1965d));
        myRedisTemplate.delete("hackers");
    }

    @Test
    public void testRemoveMemberOfZSet() {
        myRedisTemplate.addElementOfZSet("hackers", "Alan Kay", 1940);
        myRedisTemplate.addElementOfZSet("hackers", "Richard Stallman", 1953);
        myRedisTemplate.addElementOfZSet("hackers", "Yukihiro Matsumoto", 1965);
        myRedisTemplate.addElementOfZSet("hackers", "Claude Shannon", 1916);
        myRedisTemplate.addElementOfZSet("hackers", "Linus Torvalds", 1969);
        myRedisTemplate.addElementOfZSet("hackers", "Alan Turing", 1912);

        assertThat(myRedisTemplate.removeMemberOfZSet("hackers", "Yukihiro Matsumoto", "Alan Turing"), is(2L));
        assertThat(myRedisTemplate.removeMemberOfZSet("hackers", "wujunshen", "Linus Torvalds"), is(1L));
        assertThat(myRedisTemplate.removeMemberOfZSet("hackers", "wujunshen"), is(0L));
        myRedisTemplate.delete("hackers");
    }

    @Test
    public void testRemoveMemberOfZSetRange() {
        myRedisTemplate.addElementOfZSet("hackers", "Alan Kay", 1940);
        myRedisTemplate.addElementOfZSet("hackers", "Richard Stallman", 1953);
        myRedisTemplate.addElementOfZSet("hackers", "Yukihiro Matsumoto", 1965);
        myRedisTemplate.addElementOfZSet("hackers", "Claude Shannon", 1916);
        myRedisTemplate.addElementOfZSet("hackers", "Linus Torvalds", 1969);
        myRedisTemplate.addElementOfZSet("hackers", "Alan Turing", 1912);

        assertThat(myRedisTemplate.removeMemberOfZSetRange("hackers", 6, 7), is(0L));
        assertThat(myRedisTemplate.removeMemberOfZSetRange("hackers", 0, 1), is(2L));
        assertThat(myRedisTemplate.removeMemberOfZSetRange("hackers", 0, 7), is(4L));
        myRedisTemplate.delete("hackers");
    }

    @Test
    public void testRemoveMemberOfZSetRangeByScore() {
        myRedisTemplate.addElementOfZSet("hackers", "Alan Kay", 1940);
        myRedisTemplate.addElementOfZSet("hackers", "Richard Stallman", 1953);
        myRedisTemplate.addElementOfZSet("hackers", "Yukihiro Matsumoto", 1965);
        myRedisTemplate.addElementOfZSet("hackers", "Claude Shannon", 1916);
        myRedisTemplate.addElementOfZSet("hackers", "Linus Torvalds", 1969);
        myRedisTemplate.addElementOfZSet("hackers", "Alan Turing", 1912);

        assertThat(myRedisTemplate.removeMemberOfZSetRangeByScore("hackers", 2016, 2000), is(0L));
        assertThat(myRedisTemplate.removeMemberOfZSetRangeByScore("hackers", 2010, 2010), is(0L));
        assertThat(myRedisTemplate.removeMemberOfZSetRangeByScore("hackers", 2000, 2016), is(0L));
        assertThat(myRedisTemplate.removeMemberOfZSetRangeByScore("hackers", 1940, 1940), is(1L));
        assertThat(myRedisTemplate.removeMemberOfZSetRangeByScore("hackers", 1912, 1915), is(1L));
        assertThat(myRedisTemplate.removeMemberOfZSetRangeByScore("hackers", 1900, 2000), is(4L));
        myRedisTemplate.delete("hackers");
    }

    @Test
    public void testCount() {
        myRedisTemplate.addElementOfZSet("hackers", "Alan Kay", 1940);
        myRedisTemplate.addElementOfZSet("hackers", "Richard Stallman", 1953);
        myRedisTemplate.addElementOfZSet("hackers", "Yukihiro Matsumoto", 1965);
        myRedisTemplate.addElementOfZSet("hackers", "Claude Shannon", 1916);
        myRedisTemplate.addElementOfZSet("hackers", "Linus Torvalds", 1969);
        myRedisTemplate.addElementOfZSet("hackers", "Alan Turing", 1912);

        assertThat(myRedisTemplate.count("hackers", 2016, 2000), is(0L));
        assertThat(myRedisTemplate.count("hackers", 2010, 2010), is(0L));
        assertThat(myRedisTemplate.count("hackers", 2000, 2016), is(0L));
        assertThat(myRedisTemplate.count("hackers", 1940, 1940), is(1L));
        assertThat(myRedisTemplate.count("hackers", 1912, 1915), is(1L));
        assertThat(myRedisTemplate.count("hackers", 1900, 2000), is(6L));
        myRedisTemplate.delete("hackers");
    }

    @Test
    public void testGetElementOfHash() {
        myRedisTemplate.addElementOfHash("employee", "name", "wujunshen");
        myRedisTemplate.addElementOfHash("employee", "age", "39");
        myRedisTemplate.addElementOfHash("employee", "gender", "male");

        assertThat(myRedisTemplate.getElementOfHash("employee", "name"), is("wujunshen"));
        assertThat(myRedisTemplate.getElementOfHash("employee", "age"), is("39"));
        assertThat(myRedisTemplate.getElementOfHash("employee", "gender"), is("male"));

        myRedisTemplate.delete("employee");
    }

    @Test
    public void testAddAllElementOfHash() {
        Map<Object, Object> map = new HashMap<>();
        map.put("name", "wujunshen");
        map.put("age", "39");
        map.put("gender", "male");
        myRedisTemplate.addAllElementOfHash("employee", map);

        List<String> hashKeys = new ArrayList<>();
        hashKeys.add("name");
        hashKeys.add("age");

        List valueList = myRedisTemplate.getAllElementOfHash("employee", hashKeys);

        assertThat(valueList.contains("wujunshen"), is(true));
        assertThat(valueList.contains("39"), is(true));
        assertThat(valueList.contains("male"), is(false));

        Set<String> hashKeySet = new LinkedHashSet<>();
        hashKeySet.add("name");
        hashKeySet.add("age");

        valueList = myRedisTemplate.getAllElementOfHash("employee", hashKeySet);

        assertThat(valueList.contains("wujunshen"), is(true));
        assertThat(valueList.contains("39"), is(true));
        assertThat(valueList.contains("male"), is(false));

        Map<Object, Object> hashKeyMap = myRedisTemplate.getAllElementOfHash("employee");
        assertThat(hashKeyMap, equalTo(map));

        myRedisTemplate.delete("employee");
    }

    @Test
    public void testGetSizeOfHash() {
        Map<Object, Object> map = new HashMap<>();
        map.put("name", "wujunshen");
        map.put("age", "39");
        map.put("gender", "male");
        myRedisTemplate.addAllElementOfHash("employee", map);

        assertThat(myRedisTemplate.getSizeOfHash("employee"), is(3L));

        myRedisTemplate.delete("employee");
    }

    @Test
    public void testIsExisted() {
        Map<Object, Object> map = new HashMap<>();
        map.put("name", "wujunshen");
        map.put("age", "39");
        map.put("gender", "male");
        myRedisTemplate.addAllElementOfHash("employee", map);

        assertThat(myRedisTemplate.isExisted("employee", "name"), is(true));
        assertThat(myRedisTemplate.isExisted("employee", "mobile"), is(false));

        myRedisTemplate.delete("employee");
    }

    @Test
    public void testGetHashKeysBy() {
        Map<Object, Object> map = new HashMap<>();
        map.put("name", "wujunshen");
        map.put("age", "39");
        map.put("gender", "male");
        myRedisTemplate.addAllElementOfHash("employee", map);

        Set mySet = myRedisTemplate.getHashKeysBy("employee");
        assertThat(mySet.isEmpty(), is(false));
        assertThat(mySet.contains("name"), is(true));
        assertThat(mySet.contains("age"), is(true));
        assertThat(mySet.contains("gender"), is(true));

        myRedisTemplate.delete("employee");
    }

    @Test
    public void testGetHashValuesBy() {
        Map<Object, Object> map = new HashMap<>();
        map.put("name", "wujunshen");
        map.put("age", "39");
        map.put("gender", "male");
        myRedisTemplate.addAllElementOfHash("employee", map);

        List myList = myRedisTemplate.getHashValuesBy("employee");
        assertThat(myList.isEmpty(), is(false));
        assertThat(myList.contains("wujunshen"), is(true));
        assertThat(myList.contains("39"), is(true));
        assertThat(myList.contains("male"), is(true));

        myRedisTemplate.delete("employee");
    }

    @Test
    public void testDeleteElementOfHash() {
        Map<Object, Object> map = new HashMap<>();
        map.put("name", "wujunshen");
        map.put("age", "39");
        map.put("gender", "male");
        myRedisTemplate.addAllElementOfHash("employee", map);

        myRedisTemplate.deleteElementOfHash("employee", "age");
        Set<String> hashKeySet = new LinkedHashSet<>();
        hashKeySet.add("name");
        hashKeySet.add("age");
        hashKeySet.add("gender");

        List valueList = myRedisTemplate.getAllElementOfHash("employee", hashKeySet);
        assertThat(valueList.isEmpty(), is(false));
        assertThat(valueList.contains("wujunshen"), is(true));
        assertThat(valueList.contains("39"), is(false));
        assertThat(valueList.contains("male"), is(true));

        myRedisTemplate.delete("employee");
    }

    @Test
    public void testGetHashMapBy() {
        Map<Object, Object> map = new HashMap<>();
        map.put("name", "wujunshen");
        map.put("age", "39");
        map.put("gender", "male");
        myRedisTemplate.addAllElementOfHash("employee", map);

        Map hashMap = myRedisTemplate.getHashMapBy("employee");
        assertThat(hashMap.isEmpty(), is(false));
        assertThat(hashMap.containsKey("name"), is(true));
        assertThat(hashMap.containsValue("wujunshen"), is(true));
        assertThat(hashMap.containsKey("age"), is(true));
        assertThat(hashMap.containsValue("39"), is(true));
        assertThat(hashMap.containsKey("gender"), is(true));
        assertThat(hashMap.containsValue("male"), is(true));

        myRedisTemplate.delete("employee");
    }

    @Test
    public void testAddElementOfSet() {
        myRedisTemplate.addElementOfSet("sets", "1");
        myRedisTemplate.addElementOfSet("sets", "2");
        myRedisTemplate.addElementOfSet("sets", "3");
        Set mySet = myRedisTemplate.getElementOfSet("sets");

        assertThat(mySet.contains("1"), is(true));
        assertThat(mySet.contains("2"), is(true));
        assertThat(mySet.contains("3"), is(true));
        myRedisTemplate.delete("sets");
    }

    @Test
    public void testIsMember() {
        myRedisTemplate.addElementOfSet("sets", "1");
        myRedisTemplate.addElementOfSet("sets", "2");
        myRedisTemplate.addElementOfSet("sets", "3");

        assertThat(myRedisTemplate.isMember("sets", "3"), is(true));
        assertThat(myRedisTemplate.isMember("sets", "4"), is(false));

        myRedisTemplate.delete("sets");
    }

    @Test
    public void testRemoveMemberOfSet() {
        myRedisTemplate.addElementOfSet("sets", "1");
        myRedisTemplate.addElementOfSet("sets", "2");
        myRedisTemplate.addElementOfSet("sets", "3");

        assertThat(myRedisTemplate.removeMemberOfSet("sets", "3"), is(1L));
        assertThat(myRedisTemplate.removeMemberOfSet("sets", "4"), is(0L));
        assertThat(myRedisTemplate.removeMemberOfSet("sets", "5", "1"), is(1L));
        assertThat(myRedisTemplate.removeMemberOfSet("sets", "6", "7"), is(0L));

        myRedisTemplate.delete("sets");
    }

    @Test
    public void testGetSizeOfSet() {
        myRedisTemplate.addElementOfSet("sets", "1");
        myRedisTemplate.addElementOfSet("sets", "2");
        myRedisTemplate.addElementOfSet("sets", "3");

        assertThat(myRedisTemplate.getSizeOfSet("sets"), is(3L));

        myRedisTemplate.delete("sets");
    }

    @Test
    public void testGetElementOfSetBy() {
        myRedisTemplate.addElementOfSet("sets", "1");
        myRedisTemplate.addElementOfSet("sets", "2");
        myRedisTemplate.addElementOfSet("sets", "3");

        log.info("get the first element is:{}", myRedisTemplate.getElementOfSetBy("sets"));
        assertThat(myRedisTemplate.getSizeOfSet("sets"), is(2L));
        log.info("get the second element is:{}", myRedisTemplate.getElementOfSetBy("sets"));
        assertThat(myRedisTemplate.getSizeOfSet("sets"), is(1L));
        log.info("get the third element is:{}", myRedisTemplate.getElementOfSetBy("sets"));
        assertThat(myRedisTemplate.getSizeOfSet("sets"), is(0L));

        myRedisTemplate.delete("sets");
    }

    @Test
    public void testGetExpireTimeBy() {
        myRedisTemplate.setExpire("employee", "frank", 60 * 60 * 24L);
        assertThat(myRedisTemplate.getExpireSecondsBy("employee"), is(86400L));
        myRedisTemplate.setExpire("employee", "frank", 60 * 60L);
        assertThat(myRedisTemplate.getExpireMinutesBy("employee"), is(59L));
        assertThat(myRedisTemplate.getExpireHoursBy("employee"), is(0L));
        assertThat(myRedisTemplate.getExpireDaysBy("employee"), is(0L));

        myRedisTemplate.delete("employee");
    }

    @Test
    public void testRename() {
        myRedisTemplate.addElementOfList("employee", "frank");
        myRedisTemplate.rename("employee", "newEmployee");

        assertThat(myRedisTemplate.getElementOfListBy("newEmployee"), is("frank"));

        myRedisTemplate.delete("newEmployee");
    }

    @Test
    public void testGetKindOfValue() {
        myRedisTemplate.set("string", "value");
        myRedisTemplate.addElementOfList("list", "value");
        myRedisTemplate.addElementOfSet("set", "value");
        myRedisTemplate.addElementOfZSet("zset", "value", 100);
        myRedisTemplate.addElementOfHash("hash", "hashKey", "hashValue");

        assertThat(myRedisTemplate.getKindOfValue("frank"), is("none"));
        assertThat(myRedisTemplate.getKindOfValue("string"), is("string"));
        assertThat(myRedisTemplate.getKindOfValue("list"), is("list"));
        assertThat(myRedisTemplate.getKindOfValue("set"), is("set"));
        assertThat(myRedisTemplate.getKindOfValue("zset"), is("zset"));
        assertThat(myRedisTemplate.getKindOfValue("hash"), is("hash"));

        myRedisTemplate.delete("string");
        myRedisTemplate.delete("list");
        myRedisTemplate.delete("set");
        myRedisTemplate.delete("zset");
        myRedisTemplate.delete("hash");
    }

    private void initData4PipelineTest() {
        myRedisTemplate.flushDB();
        for (int i = 0; i < MAX_SIZE; i++) {
            myRedisTemplate.addElementOfList("key", i + "");
        }
    }

    @Test
    public void testUnUsePipeline() {
        initData4PipelineTest();

        long start = new Date().getTime();
        for (int i = 0; i < MAX_SIZE; i++) {
            myRedisTemplate.getElementOfListBy("key");
        }
        long end = new Date().getTime();

        log.info("unUsed pipeline cost:{}ms", (end - start));
    }

    /**
     * redis单点时忽略此方法
     */
    @Test(expected = UnsupportedOperationException.class)
    public void testUsePipeline() {
        initData4PipelineTest();

        long start = new Date().getTime();
        myRedisTemplate.executePipelined("key");
        long end = new Date().getTime();

        log.info("use pipeline cost:{}ms", (end - start));
    }

    /**
     * sort list
     * LIST结合hash的排序
     * 对应的redis客户端命令是： sort userlist by user:*->name get user:*->name
     */
    @Test
    public void testSort2() {
        myRedisTemplate.flushDB();
        myRedisTemplate.addElementOfList("userlist", "33");
        myRedisTemplate.addElementOfList("userlist", "22");
        myRedisTemplate.addElementOfList("userlist", "55");
        myRedisTemplate.addElementOfList("userlist", "11");
        myRedisTemplate.addElementOfList("userlist", "66");

        myRedisTemplate.addElementOfHash("user:66", "name", "66");
        myRedisTemplate.addElementOfHash("user:55", "name", "55");
        myRedisTemplate.addElementOfHash("user:33", "name", "33");
        myRedisTemplate.addElementOfHash("user:22", "name", "79");
        myRedisTemplate.addElementOfHash("user:11", "name", "24");
        myRedisTemplate.addElementOfHash("user:11", "set", "beijing");
        myRedisTemplate.addElementOfHash("user:22", "set", "shanghai");
        myRedisTemplate.addElementOfHash("user:33", "set", "guangzhou");
        myRedisTemplate.addElementOfHash("user:55", "set", "chongqing");
        myRedisTemplate.addElementOfHash("user:66", "set", "xi'an");

        SortQueryBuilder<Object> builder = SortQueryBuilder.sort("userlist");
        builder.alphabetical(true);//对字符串使用“字典顺序”
        List<Object> result = myRedisTemplate.sortBy(builder);
        for (Object item : result) {
            log.info("item....{}", item);
        }
    }

    /**
     * sort set
     * SET结合String的排序
     * 对应的redis 命令是 sort tom:friend:list by score:uid:* get # get uid:* get score:uid:*
     */
    @Test
    public void testSort3() {
        myRedisTemplate.flushDB();
        myRedisTemplate.addElementOfList("tom:friend:list", "123");
        myRedisTemplate.addElementOfList("tom:friend:list", "456");
        myRedisTemplate.addElementOfList("tom:friend:list", "789");
        myRedisTemplate.addElementOfList("tom:friend:list", "101");

        myRedisTemplate.set("score:uid:123", "1000");
        myRedisTemplate.set("score:uid:456", "6000");
        myRedisTemplate.set("score:uid:789", "100");
        myRedisTemplate.set("score:uid:101", "5999");

        myRedisTemplate.set("uid:123", "{'uid':123,'name':'lucy'}");
        myRedisTemplate.set("uid:456", "{'uid':456,'name':'jack'}");
        myRedisTemplate.set("uid:789", "{'uid':789,'name':'jay'}");
        myRedisTemplate.set("uid:101", "{'uid':101,'name':'jolin'}");

        SortQueryBuilder<Object> builder = SortQueryBuilder.sort("tom:friend:list");
        builder.alphabetical(true);
        builder.order(SortParameters.Order.DESC);

        List<Object> result = myRedisTemplate.sortBy(builder);
        for (Object item : result) {
            log.info("item....{}", item);
        }

        assertThat(myRedisTemplate.sortByAndStore(builder, "tom:friend:list"),
                is(1L));
        List<Object> elementOfSet = myRedisTemplate.getListBy("tom:friend:list", 0, -1);

        for (Object item : elementOfSet) {
            log.info("stored item....{}", item);
        }
    }

    @Test
    public void testGetDBSizeBy() {
        myRedisTemplate.flushDB();

        myRedisTemplate.set("key", "value");
        assertThat(myRedisTemplate.getDBSize(), is(1L));
        myRedisTemplate.set("key1", "value");
        assertThat(myRedisTemplate.getDBSize(), is(2L));
    }
}