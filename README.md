# my-springboot-starter

#### 项目介绍

工作中用到的自定义starter项目收集

#### 使用说明

**Prometheus(普罗米修斯)**

一套开源的监控&报警&时间序列数据库的组合,由SoundCloud公司开发。

[github地址](https://github.com/prometheus/prometheus)

[starter文档](https://gitee.com/darkranger/my-springboot-starter/blob/master/prometheus/README.md)

**Swagger**

一套RESTFUL接口文档，提供在线自动生成和功能测试2个功能。

[github地址](https://github.com/swagger-api/swagger-core)

[starter文档](https://gitee.com/darkranger/my-springboot-starter/blob/master/swagger/README.md)

**ElasticSearch**

ElasticSearch(简称ES)是一个基于Lucene的搜索服务。相当于一个分布式多用户功能的全文搜索引擎，且提供RESTful web接口。

[github地址](https://github.com/elastic/elasticsearch)

[5.x版本starter文档](https://gitee.com/darkranger/my-springboot-starter/blob/master/elasticsearch/5.x/README.md)

[6.x版本starter文档](https://gitee.com/darkranger/my-springboot-starter/blob/master/elasticsearch/6.x/README.md)

**Redis**

Redis是一个高性能的key-value数据库。目前作为分布式缓存，被企业应用的场景比较多

[github地址](https://github.com/antirez/redis)

[starter文档](https://gitee.com/darkranger/my-springboot-starter/blob/master/redis/README.md)

**MongoDB**

MongoDB一个基于分布式文件存储的数据库

[github地址](https://github.com/mongodb/mongo)

[starter文档](https://gitee.com/darkranger/my-springboot-starter/blob/master/mongodb/README.md)

**Orika**

Orika是一个Bean映射工具，底层采用javassist类库生成Bean映射的字节码

然后直接加载执行生成的字节码文件，因此速度很快

具体介绍和性能比较见下列网址：

http://tech.dianwoda.com/2017/11/04/gao-xing-neng-te-xing-feng-fu-de-beanying-she-gong-ju-orika/


[github地址](https://github.com/orika-mapper/orika)

[starter文档](https://gitee.com/darkranger/my-springboot-starter/blob/master/orika/README.md)

**SnowFlake(雪花算法)**

SnowFlake是twitter公司为了生成分布式ID而使用的一个算法。

产生原因:为了满足Twitter每秒上万条消息的请求，每条消息都必须被分配一个唯一的id

这些id还需一定顺序（方便客户端排序），并要求在分布式系统中，不同的机器生成的id必须不同。

[github地址](https://github.com/twitter/snowflake)

[starter文档](https://gitee.com/darkranger/my-springboot-starter/blob/master/snowflake/README.md)