package com.wujunshen.sample.service.impl;

import com.wujunshen.sample.SampleMongodbSpringBootStarterApplication;
import com.wujunshen.sample.entity.User;
import com.wujunshen.sample.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * User:  FrankWoo(吴峻申) <br>
 * Date:  2018/7/24 <br>
 * Time:  18:10 <br>
 * Email: frank_wjs@hotmail.com <br>
 */
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = SampleMongodbSpringBootStarterApplication.class)
@EnableAutoConfiguration
public class UserServiceImplTest {
    @Resource
    private UserService userService;

    private User masterUser;
    private User slaveUser;


    @Before
    public void setUp() {
        masterUser = new User();
        masterUser.setUserName("wujunshen");
        masterUser.setPassword("wujunshen");

        slaveUser = new User();
        slaveUser.setUserName("wujunshen-slave");
        slaveUser.setPassword("wujunshen-slave");
    }

    @After
    public void tearDown() {
        masterUser = null;
        slaveUser = null;
    }

    @Test
    public void getUserOfMasterDB() {
        assertThat(userService.getUserOfMasterDB("wujunshen", "wujunshen").getUserName(),
                is(masterUser.getUserName()));

        assertThat(userService.getUserOfMasterDB("wujunshen", "wujunshen").getPassword(),
                is(masterUser.getPassword()));
    }

    @Test
    public void getUserOfSlaveDB() {
        assertThat(userService.getUserOfSlaveDB("wujunshen-slave", "wujunshen-slave").getUserName(),
                is(slaveUser.getUserName()));

        assertThat(userService.getUserOfSlaveDB("wujunshen-slave", "wujunshen-slave").getPassword(),
                is(slaveUser.getPassword()));
    }

    @Test
    public void saveOfMasterDB() {
        if (!userService.existsOfMasterDB("wujunshen")) {
            userService.saveOfMasterDB(masterUser);
        }
    }

    @Test
    public void saveOfSlaveDB() {
        if (!userService.existsOfSlaveDB("wujunshen-slave")) {
            userService.saveOfSlaveDB(slaveUser);
        }
    }

    @Test
    public void existsOfMasterDB() {
        assertThat(userService.existsOfMasterDB("wujunshen"), is(true));
    }

    @Test
    public void existsOfSlaveDB() {
        assertThat(userService.existsOfSlaveDB("wujunshen-slave"), is(true));
    }
}