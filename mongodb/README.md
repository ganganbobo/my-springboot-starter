# MongoDB

#### 项目介绍

主要包含三点改进增强

1.增加多数据源配置(包括GridFs配置)

参见sample-mongodb-spring-boot-starter项目的application.properties文件中groups配置

根据下标0，1等代表各个数据源配置，如果没有设置gridFsTemplateName则不会配置GridFs

2.由于mongodb的官方spring boot starter没有对连接池的配置，因此需要扩展提供

参见sample-mongodb-spring-boot-starter项目的application.properties文件

如果觉得配置属性太多配置起来麻烦，可以不配置，缺省调用MongoGroup类中缺省属性值

3.Mongodb保存数据时，会自动增加_class字段用来关联这条数据的实体类

大数据量下会浪费空间，可通过application.properties文件中的showClass属性(设置为false)禁用：

参见mongodb-spring-boot-starter项目
在/resources/META_INF/spring.factories文件中指定自动装配类

MongoAutoConfiguration是自动装配类。

#### 使用说明

参见sample-mongodb-spring-boot-starter项目

1. 在pom文件中加入starter依赖

```
    <dependency>
        <groupId>com.wujunshen</groupId>
        <artifactId>mongodb-spring-boot-starter</artifactId>
        <version>0.0.1-SNAPSHOT</version>
    </dependency>
```

2. 在application.properties文件中加入

```
    spring.data.mongodb.groups[0].host=localhost
    spring.data.mongodb.groups[0].port=27017
    spring.data.mongodb.groups[0].database=master
    spring.data.mongodb.groups[0].mongoTemplateName=masterMongoTemplate
    spring.data.mongodb.groups[0].gridFsTemplateName=masterGridFsTemplate
    spring.data.mongodb.groups[0].min-connections-per-host=1
    spring.data.mongodb.groups[0].max-connections-per-host=100
    spring.data.mongodb.groups[0].threads-allowed-to-block-for-connection-multiplier=5
    spring.data.mongodb.groups[0].server-selection-timeout=30000
    spring.data.mongodb.groups[0].max-wait-time=120000
    spring.data.mongodb.groups[0].max-connection-idle-time=0
    spring.data.mongodb.groups[0].max-connection-life-time=0
    spring.data.mongodb.groups[0].connect-timeout=10000
    spring.data.mongodb.groups[0].socket-timeout=0
    spring.data.mongodb.groups[0].socket-keep-alive=false
    spring.data.mongodb.groups[0].ssl-enabled=false
    spring.data.mongodb.groups[0].ssl-invalid-host-name-allowed=false
    spring.data.mongodb.groups[0].always-use-m-beans=false
    spring.data.mongodb.groups[0].heartbeat-socket-timeout=20000
    spring.data.mongodb.groups[0].heartbeat-connect-timeout=20000
    spring.data.mongodb.groups[0].min-heartbeat-frequency=500
    spring.data.mongodb.groups[0].heartbeat-frequency=10000
    spring.data.mongodb.groups[0].local-threshold=15
    spring.data.mongodb.groups[0].showClass=false
    spring.data.mongodb.groups[1].host=localhost
    spring.data.mongodb.groups[1].port=27017
    spring.data.mongodb.groups[1].database=slave
    spring.data.mongodb.groups[1].mongoTemplateName=slaveMongoTemplate
    spring.data.mongodb.groups[1].gridFsTemplateName=slaveGridFsTemplate
    spring.data.mongodb.groups[1].min-connections-per-host=10
    spring.data.mongodb.groups[1].max-connections-per-host=1000
    spring.data.mongodb.groups[1].threads-allowed-to-block-for-connection-multiplier=5
    spring.data.mongodb.groups[1].server-selection-timeout=30000
    spring.data.mongodb.groups[1].max-wait-time=120000
    spring.data.mongodb.groups[1].max-connection-idle-time=10
    spring.data.mongodb.groups[1].max-connection-life-time=120
    spring.data.mongodb.groups[1].connect-timeout=10000
    spring.data.mongodb.groups[1].socket-timeout=0
    spring.data.mongodb.groups[1].socket-keep-alive=false
    spring.data.mongodb.groups[1].ssl-enabled=false
    spring.data.mongodb.groups[1].ssl-invalid-host-name-allowed=false
    spring.data.mongodb.groups[1].always-use-m-beans=false
    spring.data.mongodb.groups[1].heartbeat-socket-timeout=20000
    spring.data.mongodb.groups[1].heartbeat-connect-timeout=20000
    spring.data.mongodb.groups[1].min-heartbeat-frequency=500
    spring.data.mongodb.groups[1].heartbeat-frequency=10000
    spring.data.mongodb.groups[1].local-threshold=150
```

注意两点

a. mongoTemplateName和gridFsTemplateName值必须对应类中调用MongoTemplate类和GridFsTemplate类的变量名

以及Qualifier注解中名字

比如在UserServiceImpl类中

```
    @Resource
    @Qualifier("masterMongoTemplate")
    private MongoTemplate masterMongoTemplate;

    @Resource
    @Qualifier("slaveMongoTemplate")
    private MongoTemplate slaveMongoTemplate;

    @Resource
    @Qualifier("masterGridFsTemplate")
    private GridFsTemplate masterGridFsTemplate;

    @Resource
    @Qualifier("slaveGridFsTemplate")
    private GridFsTemplate slaveGridFsTemplate;
```

这4个变量和Qualifier注解名必须和application.properties文件中mongoTemplateName和gridFsTemplateName值一致

b. 从min-connections-per-host开始的属性可以不配置，包括showClass属性(缺省为true)。理由之前已说明

3. 启动sample-mongodb-spring-boot-starter应用后，运行test包下的UserServiceImplTest类

如下图

![输入图片说明](https://images.gitee.com/uploads/images/2018/0724/192112_b3873754_43183.png "Snip20180724_5.png")

因为spring.data.mongodb.groups[0].showClass设置为false，可见下列两张图看见master库中user没有_class字段

![输入图片说明](https://images.gitee.com/uploads/images/2018/0724/192154_3479237c_43183.png "Snip20180724_3.png")

![输入图片说明](https://images.gitee.com/uploads/images/2018/0724/192225_9117f67f_43183.png "Snip20180724_4.png")